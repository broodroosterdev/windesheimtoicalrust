## Building Stage ##
FROM messense/rust-musl-cross:armv7-musleabihf as builder
RUN rustup default nightly 
RUN rustup target add armv7-unknown-linux-musleabihf
WORKDIR /build
ADD ./api .
RUN cargo build --release --target armv7-unknown-linux-musleabihf


## Running stage ##
FROM arm32v7/alpine:3
RUN apk --update add tzdata memcached
RUN cp /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
EXPOSE 8000
COPY --from=builder /build/target/armv7-unknown-linux-musleabihf/release/api .
ADD start.sh .
RUN chmod +x start.sh
CMD ["./start.sh"]