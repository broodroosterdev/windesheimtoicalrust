extern crate ics;
extern crate reqwest;

use crate::les::Les;
use crate::rooster::RoosterType;

fn get_json(url: String) -> Result<String, String> {
    let req_url: &str = &*url;
    let mut request_result = reqwest::get(req_url).map_err(|e| e.to_string())?;
    let text_result = request_result.text().map_err(|e| e.to_string())?;
    return Ok(text_result);
}

fn get_type_string(given_type: &RoosterType) -> &'static str {
    return match given_type {
        RoosterType::Docent => "docent/",
        RoosterType::Klas => "klas/",
        RoosterType::Vak => "vak/",
    };
}

pub fn get_lessen(klascode: &String, given_type: &RoosterType) -> Result<Vec<Les>, String> {
    let mut url: String = "http://api.windesheim.nl/api/".to_owned();
    let type_string: &str = get_type_string(&given_type);
    let closing_url: String = "/Les".to_owned();
    url.push_str(&type_string);
    url.push_str(&klascode);
    url.push_str(&closing_url);
    let lessen = parse_lessen(get_json(url)?)?;
    return if lessen.is_empty() {
        Err("No lessons found for this code".parse().unwrap())
    } else {
        Ok(lessen)
    };
}

fn parse_lessen(json: String) -> Result<Vec<Les>, String> {
    let rooster = serde_json::from_str(&json).map_err(|e| e.to_string())?;
    return Ok(rooster);
}
