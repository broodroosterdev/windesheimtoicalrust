use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::path::Path;
use memcache::*;
use chrono::{Duration, Local};

use crate::{api, klas};
use crate::rooster::{Rooster, RoosterType};
use serde_json::Error;

#[derive(Serialize, Deserialize, Clone)]
pub struct CachedRooster {
    pub(crate) rooster: Rooster,
    expiration: i64,
}

impl CachedRooster {
    pub fn getclient() -> Client{
        let client = Client::connect("memcache://127.0.0.1:11211").unwrap();
        return client;
    }
    pub fn get(klascode: &String) -> Result<Rooster, String> {
        let mut client = CachedRooster::getclient();
        let cached_rooster: Result<Rooster, Error> = serde_json::from_str(client.get::<String>(klascode.to_string().as_ref()).unwrap().unwrap().to_string().as_str());
        return if cached_rooster.is_err() {
            Err("No CachedRooster found in cache".parse().unwrap())
        } else {
                Ok(cached_rooster.unwrap().clone())
        };
    }

    pub(crate) fn create_and_add(klascode: &String, roostertype: &RoosterType) -> Result<Rooster, String> {
        let lessen = api::get_lessen(klascode, roostertype)?;
        let rooster = Rooster::new(lessen, klascode.clone(), *roostertype);
        CachedRooster::put(serde_json::to_string(&rooster.clone()).unwrap(), klascode.to_string())?;
        Ok(rooster.clone())
    }

    fn put(rooster: String, klascode: String) -> Result<(), String> {
        let mut client = CachedRooster::getclient();
        client.add(klascode.as_ref(), rooster, 10 * 60).map_err(|err| {err.to_string()})?;
        return Ok(());
    }

    pub fn has(client: &mut Client, klascode: &String, roostertype: &RoosterType) -> Result<bool, String> {
        let cached_rooster = client.get::<String>(&klascode).unwrap();
        return if cached_rooster.is_none() {
                Ok(false)
            } else {
                let rooster: Result<Rooster, Error> = serde_json::from_str(cached_rooster.unwrap().as_str());
                Ok(rooster.unwrap().clone().roostertype == *roostertype)
            }
    }
}