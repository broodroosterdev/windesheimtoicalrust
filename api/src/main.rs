#![feature(proc_macro_hygiene, decl_macro)]
#![feature(in_band_lifetimes)]
extern crate reqwest;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket_failure::errors::*;

use rooster::*;

mod les;
mod api;
mod rooster;
mod cache;

#[get("/<code>")]
fn default(code: String) -> ApiResult<IcalResponder> {
    let rooster = Rooster::from_klascode(&code, &RoosterType::Klas).publish_error()?;
    let responder = rooster.to_responder().publish_error()?;
    Ok(responder)
}

#[get("/klas/<code>")]
fn klas(code: String) -> ApiResult<IcalResponder> {
    let rooster = Rooster::from_klascode(&code, &RoosterType::Klas).publish_error()?;
    let responder = rooster.to_responder().publish_error()?;
    Ok(responder)
}

#[get("/docent/<code>")]
fn docent(code: String) -> ApiResult<IcalResponder> {
    let rooster = Rooster::from_klascode(&code, &RoosterType::Docent).publish_error()?;
    let responder = rooster.to_responder().publish_error()?;
    Ok(responder)
}

#[get("/vak/<code>")]
fn vak(code: String) -> ApiResult<IcalResponder> {
    let rooster = Rooster::from_klascode(&code, &RoosterType::Vak).publish_error()?;
    let responder = rooster.to_responder().publish_error()?;
    Ok(responder)
}

fn main() {
    rocket::ignite()
        .mount("/", routes![default])
        .mount("/", routes![klas])
        .mount("/", routes![docent])
        .mount("/", routes![vak])
        .launch();
}