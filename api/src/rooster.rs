extern crate rocket;

use std::borrow::Borrow;

use chrono::{Local, NaiveDateTime, SecondsFormat, Utc};
use chrono::offset::TimeZone;
use ics::{Event, ICalendar};
use ics::properties::{Categories, DtEnd, DtStart, Location, Status, Summary};
use rayon::prelude::*;
use rocket::http::ContentType;
use rocket::http::Header;
use rocket::response::Content;
use rocket::response::Responder;
use memcache::{FromMemcacheValue, ToMemcacheValue};
use crate::cache::CachedRooster;
use crate::les::Les;

#[derive(Responder)]
#[response(status = 200, content_type = "text/calendar")]
pub struct IcalResponder {
    inner: Content<String>,
    header: Header<'static>,
}

#[derive(Copy, Clone, Serialize, Deserialize, PartialEq, Debug)]
pub enum RoosterType {
    Klas,
    Docent,
    Vak,
}

#[derive(Serialize, Deserialize)]
pub struct Rooster {
    lessen: Vec<Les>,
    pub(crate) klascode: String,
    pub(crate) roostertype: RoosterType,
}

impl Clone for Rooster {
    fn clone(&self) -> Rooster {
        Rooster {
            lessen: self.lessen.to_vec(),
            klascode: self.klascode.to_string(),
            roostertype: self.roostertype,
        }
    }
}

impl Rooster {
    pub fn new(lessen: Vec<Les>, klascode: String, roostertype: RoosterType) -> Rooster {
        Rooster {
            lessen,
            klascode,
            roostertype,
        }
    }

    pub fn from_klascode(klascode: &String, roostertype: &RoosterType) -> Result<Rooster, String> {
        let mut client = CachedRooster::getclient();
        let has_rooster = CachedRooster::has(&mut client, klascode, roostertype)?;
        return if has_rooster {
            let cached_rooster = CachedRooster::get(klascode)?;
            Ok(cached_rooster)
        } else {
            let new_rooster = CachedRooster::create_and_add(klascode, roostertype)?;
            Ok(new_rooster)
        };
    }

    pub fn to_ical(&self) -> Result<ICalendar, String> {
        return if self.lessen.is_empty() {
            Err("No lessons found in Rooster".parse().unwrap())
        } else {
            let mut ical = ICalendar::new("2.0", "-//WindesheimtoiCal//WindesheimiCal 1.0//EN");
            let events = self.lessen_to_events();
            for event in events {
                ical.add_event(event);
            }
            Ok(ical)
        };
    }

    fn lessen_to_events(&self) -> Vec<Event> {
        let events: Vec<Event> = self.lessen.par_iter()
            .map(|les| self.les_to_event(&les))
            .collect();
        return events;
    }

    fn les_to_event(&self, les: &Les) -> Event {
        let mut event = Event::new(les.id.to_string(), format_date(les.roosterdatum.to_string()));
        event.push(DtStart::new(
            format_date(create_date_time(les.starttijd))
        ));
        event.push(DtEnd::new(
            format_date(create_date_time(les.eindtijd))
        ));
        event.push(Status::confirmed());
        event.push(Categories::new(les.vaknaam.to_string()));
        event.push(Summary::new(les.commentaar.to_string()));
        event.push(Location::new(les.lokaal.to_string()));
        return event;
    }

    pub fn to_responder(&self) -> Result<IcalResponder, String> {
        let icalendar = self.to_ical()?;
        let header = Header::new("Content-Disposition", String::from("attachment; filename=\"{}.ics\"").replace("{}", &self.klascode));
        Ok(IcalResponder {
            inner: Content(ContentType::Calendar, icalendar.to_string()),
            header,
        })
    }
}

fn format_date(string: String) -> String {
    return string.replace("-", "").replace(":", "");
}

pub fn create_date_time(time: u64) -> String {
    Local.from_local_datetime(NaiveDateTime::from_timestamp((time / 1000) as i64, 0).borrow()).unwrap()
        .with_timezone(&Utc)
        .to_rfc3339_opts(SecondsFormat::Secs, true)
}