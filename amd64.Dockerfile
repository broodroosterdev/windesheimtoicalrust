## Building Stage ##
FROM messense/rust-musl-cross:x86_64-musl as builder
RUN rustup default nightly 
RUN rustup target add x86_64-unknown-linux-musl
WORKDIR /build
ADD ./api .
RUN cargo build --release --target x86_64-unknown-linux-musl


## Running stage ##
FROM amd64/alpine:3
RUN apk --update add tzdata memcached
RUN cp /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
EXPOSE 8000
COPY --from=builder /build/target/x86_64-unknown-linux-musl/release/api .
ADD start.sh .
RUN chmod +x start.sh
CMD ["./start.sh"]

